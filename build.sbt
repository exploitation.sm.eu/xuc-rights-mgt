import AssemblyKeys._
import play.PlayImport.PlayKeys._

name := "xuc-rights-mgt"

version := "1.1.5"

lazy val root = (project in file(".")).enablePlugins(PlayScala).enablePlugins(DockerPlugin)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "xivo" %% "play-authentication" % "1.9.0",
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "org.dbunit" % "dbunit" % "2.4.7"
)

assemblySettings

mergeStrategy in assembly <<= (mergeStrategy in assembly) {
  (old) => {
    case PathList("application.conf") => MergeStrategy.discard
    case PathList("logback.xml") => MergeStrategy.discard
    case PathList("org", "apache", "commons", xs@_*) => MergeStrategy.first
    case PathList("play", xs@_*) => MergeStrategy.first
    case x => old(x)
  }
}

jarName in assembly := "xuc-rights-mgt.jar"

artifactName in playPackageAssets := {(_, _, _) => "xuc-rights-mgt-assets.jar"}

test in assembly := {}

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

parallelExecution in Test := false

publishArtifact in (Compile, packageDoc) := false

fullClasspath in assembly += file("target/xuc-rights-mgt-assets.jar")

maintainer in Docker := "Jean Aunis <jaunis@avencall.com>"

dockerBaseImage := "java:openjdk-8u66-jdk"

dockerRepository := Some("xivoxc")

dockerExposedPorts := Seq(9000)

dockerEntrypoint := Seq("bin/xuc-rights-mgt-docker")

dockerExposedVolumes := Seq("/var/log")
