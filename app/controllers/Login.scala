package controllers

import model.Right
import models.authentication.{LoginDriver, XivoDriver}
import play.api._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._
import views.html

object Login extends Controller {

  val logger = Logger(getClass.getName)
  val loginDriver = new LoginDriver()
  val xivoDriver = new XivoDriver()

  def login = Action { implicit request => Ok(html.login(loginForm)) }

  val loginForm = Form(tuple(
      "login" -> text,
      "mdp" -> text))

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.login(formWithErrors)),
      loginMdp => loginMdp match {
        case (login, pwd) => loginDriver.authenticate(login, pwd) match {
          case Some(user) => {
            Redirect(routes.Pages.index()).withSession("username" -> user.username, "isSuperAdmin" -> "true")
          }
          case None => {
            (for {
              rights <- Right.forUser(login)
              user <- xivoDriver.authenticate(login,pwd)
            } yield Redirect(routes.Pages.index()).withSession("username" -> login)) getOrElse  BadRequest(html.login(loginForm.withGlobalError("Login ou mot de passe invalide")))
          }
        }
      }
    )
  }

  def logout = Action { implicit request =>
    logger.info(s"L'utilisateur ${request.session.get("username")} s'est déconnecté")
    Redirect(routes.Login.login).withNewSession
  }
}