package controllers

import model.User
import play.api.libs.json.Json
import play.api.mvc.{Call, Controller}

object Users extends Controller with Secured {

  def all = IsAuthenticated(false, request => Ok(Json.toJson(User.list.map(u => u.toJson))))

  override val loginRoute: Call = routes.Login.login()
}
