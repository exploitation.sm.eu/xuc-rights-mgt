package controllers

import model._
import play.api.libs.json.Json
import play.api.mvc.{Call, Controller}

object XivoObjects extends Controller with Secured {

  def getQueues = IsAuthenticated(false, request => {
    if(request.session.get("isSuperAdmin") == Some("true"))
      Ok(Json.toJson(Queue.all()))
    else {
      val right = Right.forUser(request.session.get("username").get)
      right match {
        case Some(SupervisorRight(queueIds, _, _)) => Ok(Json.toJson(Queue.withIds(queueIds)))
        case _ => Forbidden
      }
    }
  })

  def getGroups = IsAuthenticated(false, request => {
    if(request.session.get("isSuperAdmin") == Some("true"))
      Ok(Json.toJson(AgentGroup.all()))
    else {
      val right = Right.forUser(request.session.get("username").get)
      right match {
        case Some(SupervisorRight(_, groupIds, _)) => Ok(Json.toJson(AgentGroup.withIds(groupIds)))
        case _ => Forbidden
      }
    }
  })

  def getIncalls = IsAuthenticated(false, request => {
    if(request.session.get("isSuperAdmin") == Some("true"))
      Ok(Json.toJson(Incall.all()))
    else {
      val right = Right.forUser(request.session.get("username").get)
      right match {
        case Some(SupervisorRight(_, _, incallIds)) => Ok(Json.toJson(Incall.withIds(incallIds)))
        case _ => Forbidden
      }
    }
  })

  def getXivoUsers = IsAuthenticated(false, request => Ok(Json.toJson(XivoUser.all().map(u => u.toJson))))

  override val loginRoute: Call = routes.Login.login()
}
