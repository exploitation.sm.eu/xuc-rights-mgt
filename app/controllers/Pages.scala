package controllers

import play.api.mvc.{Call, Controller}

object Pages extends Controller with Secured {

  def index = IsAuthenticated(true, request => Ok(views.html.index()))

  override val loginRoute: Call = routes.Login.login()
}
