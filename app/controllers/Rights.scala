package controllers

import model.Right
import play.api.libs.json.Json
import play.api.mvc.{Action, Call, Controller}

object Rights extends Controller with Secured {

  def getRights(login: String) = Action {
    Right.forUser(login) match {
      case None => NotFound
      case Some(r) => Ok(r.toJson)
    }
  }

  def createOrEditRight(login: String) = IsAuthenticated(false, implicit request =>
    request.body.asJson match {
      case None => BadRequest(Json.toJson("The request body cannot be empty"))
      case Some(value) => {
        try {
          val right = Right.fromJson(value)
          if(request.session.get("isSuperAdmin") == Some("true") || (Right.userCanModifyUser(request.session.get("username").get, login) && Right.userCanCreateRight(request.session.get("username").get, right))) {
            Right.createOrReplace(login, right)
            Ok("")
          } else {
            BadRequest(Json.toJson("Vous n'êtes pas autorisé à créer ce droit"))
          }
        } catch {
          case e: IllegalArgumentException => BadRequest(Json.toJson("Données invalides"))
        }
      }
    })

  def deleteRight(login: String) = IsAuthenticated(false, request => {
    if(request.session.get("isSuperAdmin") == Some("true") || Right.userCanModifyUser(request.session.get("username").get, login)) {
      Right.deleteByLogin(login)
      Ok("")
    } else {
      BadRequest(Json.toJson("Vous n'êtes pas autorisé à supprimer ce droit"))
    }
  })

  override val loginRoute: Call = routes.Login.login()
}
