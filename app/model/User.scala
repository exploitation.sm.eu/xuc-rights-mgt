package model

import play.api.db.DB
import play.api.libs.json.Json
import anorm._
import anorm.SqlParser._
import play.api.Play.current

case class User(name: String, firstName: String, login: String, profile: String) {
  def toJson = Json.obj("name" -> name, "firstname" -> firstName, "login" -> login, "profile" -> profile)
}

object User {
  val partialUser = get[String]("login") ~ get[String]("type") map {
    case login ~ profile => (login, profile)
  }

  def fullUser(login: String, profile: String) = get[String]("lastname") ~ get[String]("firstname") map {
    case lastname ~ firstname => User(lastname, firstname, login, profile)
  }

  def list: List[User] = DB.withConnection("default") { defaultConn =>
    SQL("SELECT login, type::varchar AS type FROM users").as(partialUser *)(defaultConn).flatMap(t => DB.withConnection("xivo") { xivoConn =>
      SQL("SELECT lastname, firstname FROM userfeatures WHERE loginclient = {login}").on('login -> t._1).as(fullUser(t._1, t._2) *)(xivoConn).headOption
    })
  }
}
