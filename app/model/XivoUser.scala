package model

import play.api.db.DB
import play.api.Play.current
import play.api.libs.json.{Json, JsValue}
import anorm._
import anorm.SqlParser._

case class XivoUser(firstName: String, lastName: Option[String], login: String) {
  def toJson: JsValue = Json.obj("firstname" -> firstName, "lastname" -> lastName, "login" -> login)
}

object XivoUser {

  val simple = get[String]("firstname") ~ get[Option[String]]("lastname") ~ get[String]("loginclient") map {
    case firstName ~ lastName ~ login => XivoUser(firstName, lastName, login)
  }

  def all(): List[XivoUser] = DB.withConnection("xivo") { implicit c =>
    SQL("SELECT firstname, lastname, loginclient FROM userfeatures WHERE loginclient IS NOT NULL AND loginclient != ''").as(simple *)
  }
}
