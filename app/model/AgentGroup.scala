package model

import anorm.SqlParser._
import anorm._
import play.api.db.DB
import play.api.Play.current
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class AgentGroup(id: Option[Long], name: String)

trait AgentGroupFactory {
  def all(): List[AgentGroup]
  def withIds(ids: List[Int]): List[AgentGroup]
}

object AgentGroup extends AgentGroupFactory {

  implicit val agGroupWrites = (
    (__ \ "id").write[Option[Long]] and
      (__ \ "name").write[String])(unlift(AgentGroup.unapply))

  val simple = get[Long]("id") ~ get[String]("name") map {
    case id ~ name => AgentGroup(Some(id), name)
  }

  def all(): List[AgentGroup] = DB.withConnection("xivo") { implicit c =>
    SQL("select  id, name from agentgroup").as(simple *)
  }

  override def withIds(ids: List[Int]): List[AgentGroup] = DB.withConnection("xivo") { implicit c =>
    if(ids.isEmpty) List()
    else SQL("SELECT id, name FROM agentgroup WHERE id IN ({ids})").on('ids -> ids.asInstanceOf[Seq[Int]]).as(simple *)
  }
}