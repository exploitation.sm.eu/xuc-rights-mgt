package model
import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Queue(id: Option[Long], name: String)

trait QueueFactory {
  def all(): List[Queue]

  def withIds(ids: List[Int]): List[Queue]
}

object Queue extends QueueFactory {

  override def withIds(ids: List[Int]): List[Queue] = DB.withConnection("xivo") { implicit c =>
    if(ids.isEmpty) List()
    else SQL("SELECT id, name FROM queuefeatures WHERE id IN ({ids})").on('ids -> ids.asInstanceOf[Seq[Int]]).as(simple *)
  }

  implicit val queueWrites = (
    (__ \ "id").write[Option[Long]] and
      (__ \ "name").write[String])(unlift(Queue.unapply))

  val simple = get[Long]("id") ~ get[String]("name") map {
    case id ~ name => Queue(Some(id), name)
  }
  override def all(): List[Queue] = DB.withConnection("xivo") { implicit c =>
    SQL("SELECT id, name FROM queuefeatures").as(simple *)
  }
}
