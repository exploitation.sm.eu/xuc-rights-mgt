package model

import anorm.SqlParser._
import anorm._
import play.api.db.DB
import play.api.Play.current
import play.api.libs.functional.syntax._
import play.api.libs.json._


case class Incall(id: Option[Long], number: String)


trait IncallFactory {
  def all(): List[Incall]

  def withIds(ids: List[Int]): List[Incall]
}

object Incall extends IncallFactory {
  override def withIds(ids: List[Int]): List[Incall] = DB.withConnection("xivo") { implicit c =>
    if(ids.isEmpty) List()
    else SQL("SELECT  id, exten FROM incall WHERE id IN ({ids})").on('ids -> ids.asInstanceOf[Seq[Int]]).as(simple *)
  }


  implicit val agGroupWrites = (
    (__ \ "id").write[Option[Long]] and
      (__ \ "number").write[String])(unlift(Incall.unapply))

  val simple = get[Long]("id") ~ get[String]("exten") map {
    case id ~ number => Incall(Some(id), number)
  }



  def all(): List[Incall] = DB.withConnection("xivo") { implicit c =>
    SQL("select  id, exten from incall")
      .as(simple *)
  }

}