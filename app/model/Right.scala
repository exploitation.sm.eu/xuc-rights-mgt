package model

import java.sql.Connection
import java.util.Date

import anorm._
import anorm.SqlParser._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.Logger
import play.api.Play.current
import play.api.db.DB
import play.api.libs.json.{JsArray, JsValue, Json}


abstract class Right(name: String) {
  def data: JsValue

  def toJson: JsValue = Json.obj("type" -> name, "data" -> data)

  def insert(login: String)(implicit c: Connection): Unit = {
    val userId = SQL("INSERT INTO users(login, type) VALUES ({login}, {type}::user_type)").on('login -> login, 'type -> name).executeInsert[Option[Long]]()
    specificInsert(userId.get)
  }

  def specificInsert(userId: Long)(implicit c: Connection)
}

case class AdminRight() extends Right("admin") {
  override def data: JsValue = Json.obj()

  override def specificInsert(userId: Long)(implicit c: Connection): Unit = Unit
}
case class SupervisorRight(queueIds: List[Int], groupIds: List[Int], incallIds: List[Int]) extends Right("supervisor") {
  override def data: JsValue = Json.obj("queueIds" -> queueIds, "groupIds" -> groupIds, "incallIds" -> incallIds)

  override def specificInsert(userId: Long)(implicit c: Connection): Unit = {
    Right.insertQueueRights(userId.toInt, queueIds)
    Right.insertGroupRights(userId.toInt, groupIds)
    Right.insertIncallRights(userId.toInt, incallIds)
  }
}

case class TeacherRight(queueIds: List[Int], groupIds: List[Int], incallIds: List[Int], start: DateTime, end: DateTime) extends Right("teacher") {
  override def data: JsValue = Json.obj("queueIds" -> queueIds, "groupIds" -> groupIds, "incallIds" -> incallIds,
    "start" -> start.toString(TeacherRight.formatter), "end" -> end.toString(TeacherRight.formatter))

  override def specificInsert(userId: Long)(implicit c: Connection): Unit = {
    SQL("UPDATE users SET validity_start = {start}, validity_end = {end} WHERE id = {userId}")
      .on('start -> start.toDate, 'end -> end.toDate, 'userId -> userId).executeUpdate()
    Right.insertQueueRights(userId.toInt, queueIds)
    Right.insertGroupRights(userId.toInt, groupIds)
    Right.insertIncallRights(userId.toInt, incallIds)
  }
}

object Right {
  val logger = Logger.logger

  def userCanCreateRight(modifyingUser: String, right: Right): Boolean = {
    forUser(modifyingUser) match {
      case Some(SupervisorRight(userQueueIds, userGroupIds, userIncallIds)) => right match {
        case TeacherRight(queueIds, groupIds, incallIds, _, _) => queueIds.forall(userQueueIds.contains(_)) &&
          groupIds.forall(userGroupIds.contains(_)) &&
          incallIds.forall(userIncallIds.contains(_))
        case _ => false
      }
      case _ => false
    }

  }

  def userCanModifyUser(modifyingUser: String, modifiedUser: String): Boolean = {
    forUser(modifiedUser) match {
      case (Some(AdminRight()) | Some(SupervisorRight(_, _, _))) => return false
      case _ =>
    }
    forUser(modifyingUser) match {
      case Some(SupervisorRight(_, _, _)) => true
      case _ => false
    }
  }

  def deleteByLogin(login: String) = DB.withConnection { implicit c =>
    SQL("DELETE FROM rights WHERE user_id IN (SELECT id FROM users WHERE login = {login})").on('login -> login).executeUpdate()
    SQL("DELETE FROM users WHERE login = {login}").on('login -> login).executeUpdate()
  }

  def fromJson(value: JsValue): Right = (value \ "type").as[String] match {
    case "admin" => AdminRight()
    case "supervisor" => SupervisorRight((value \ "data" \ "queueIds").asOpt[JsArray].getOrElse(JsArray()).value.map(v => v.as[Int]).toList,
      (value \ "data" \ "groupIds").asOpt[JsArray].getOrElse(JsArray()).value.map(v => v.as[Int]).toList,
      (value \ "data" \ "incallIds").asOpt[JsArray].getOrElse(JsArray()).value.map(v => v.as[Int]).toList)
    case "teacher" => TeacherRight((value \ "data" \ "queueIds").asOpt[JsArray].getOrElse(JsArray()).value.map(v => v.as[Int]).toList,
      (value \ "data" \ "groupIds").asOpt[JsArray].getOrElse(JsArray()).value.map(v => v.as[Int]).toList,
      (value \ "data" \ "incallIds").asOpt[JsArray].getOrElse(JsArray()).value.map(v => v.as[Int]).toList,
      (value \ "data" \ "start").as[String], (value \ "data" \ "end").as[String])
  }

  private def cleanUserRights(login: String)(implicit c: Connection) = {
    SQL("DELETE FROM rights WHERE user_id IN (SELECT id FROM users WHERE login = {login})").on('login -> login).executeUpdate()
    SQL("DELETE FROM users WHERE login = {login}").on('login -> login).executeUpdate()
  }

  def createOrReplace(login: String, right: Right) = DB.withConnection { implicit c =>
    cleanUserRights(login)
    right.insert(login)
  }

  def simpleInt(name: String) = get[Int](name)

  val baseUser = get[String]("type") ~ get[Option[Date]]("validity_start") ~ get[Option[Date]]("validity_end") map {
    case thetype ~ start ~ end => (thetype, start, end)
  }

  def forUser(login: String): Option[Right] = DB.withConnection { implicit c =>
    SQL("SELECT type::varchar AS type, validity_start, validity_end FROM users WHERE login = {login}").on('login -> login).as(baseUser *).headOption match {
      case None => None
      case Some(("admin", _, _)) => Some(AdminRight())
      case Some(("supervisor", _, _)) => Some(SupervisorRight(queueIdsForUser(login), groupIdsForUser(login), incallIdsForUser(login)))
      case Some(("teacher", start, end)) => Some(TeacherRight(queueIdsForUser(login), groupIdsForUser(login), incallIdsForUser(login), new DateTime(start.get), new DateTime(end.get)))
    }
  }

  private def queueIdsForUser(login: String)(implicit c: Connection): List[Int] = categoryIdForUserAndType(login, "queue")

  private def groupIdsForUser(login: String)(implicit c: Connection): List[Int] = categoryIdForUserAndType(login, "agentgroup")

  def incallIdsForUser(login: String)(implicit c: Connection): List[Int] = categoryIdForUserAndType(login, "incall")

  private def categoryIdForUserAndType(login: String, category: String)(implicit c: Connection): List[Int] =
    SQL("SELECT category_id FROM rights r JOIN users u ON u.id = r.user_id WHERE u.login = {login} AND r.category = {category}::right_type")
      .on('login -> login, 'category -> category).as(simpleInt("category_id") *)

  private def insertCategoryRights(userId: Int, categoryName: String, categoryIds: List[Int])(implicit c: Connection) =
    categoryIds.foreach(id => SQL("INSERT INTO rights(user_id, category, category_id) VALUES ({user_id}, {category}::right_type, {category_id})")
      .on('user_id -> userId, 'category -> categoryName, 'category_id -> id).executeUpdate())

  def insertQueueRights(userId: Int, queueIds: List[Int])(implicit c: Connection) = insertCategoryRights(userId, "queue", queueIds)

  def insertGroupRights(userId: Int, groupIds: List[Int])(implicit c: Connection) = insertCategoryRights(userId, "agentgroup", groupIds)

  def insertIncallRights(userId: Int, incallIds: List[Int])(implicit c: Connection) = insertCategoryRights(userId, "incall", incallIds)
}

object TeacherRight {

  val formatter = DateTimeFormat.forPattern("yyyy-MM-dd")

  def apply(queueIds: List[Int], groupIds: List[Int], incallIds: List[Int], start: String, end: String): TeacherRight =
    TeacherRight(queueIds, groupIds, incallIds, formatter.parseDateTime(start), formatter.parseDateTime(end).withTime(23, 59, 59, 0))
}