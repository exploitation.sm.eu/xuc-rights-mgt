# --- !Ups
CREATE TYPE user_type AS ENUM('admin', 'supervisor');

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    login VARCHAR(64),
    "type" user_type
);

CREATE TABLE group_rights(
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id),
    group_id INTEGER
);

CREATE TABLE queue_rights(
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id),
    queue_id INTEGER
);

# --- !Downs
DROP TABLE IF EXISTS users CASCADE;
DROP TYPE IF EXISTS user_type;
DROP TABLE IF EXISTS group_rights CASCADE;
DROP TABLE IF EXISTS queue_rights CASCADE;

