# --- !Ups
ALTER TYPE user_type ADD VALUE 'teacher';
ALTER TABLE users ADD COLUMN validity_start TIMESTAMP WITHOUT TIME ZONE;
ALTER TABLE users ADD COLUMN validity_end TIMESTAMP WITHOUT TIME ZONE;

# --- !Downs
ALTER TABLE users DROP COLUMN validity_start;
ALTER TABLE users DROP COLUMN validity_end;
