DROP TABLE IF EXISTS users CASCADE;
DROP TYPE IF EXISTS user_type;

CREATE TYPE user_type AS ENUM('admin', 'supervisor', 'teacher');

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    login VARCHAR(64),
    "type" user_type,
    validity_start TIMESTAMP WITHOUT TIME ZONE,
    validity_end TIMESTAMP WITHOUT TIME ZONE
);