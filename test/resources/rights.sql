DROP TABLE IF EXISTS rights;
DROP TYPE IF EXISTS right_type;

CREATE TYPE right_type AS ENUM('queue', 'agentgroup', 'incall');

CREATE TABLE rights (
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id) NOT NULL,
    category right_type NOT NULL,
    category_id INTEGER NOT NULL
);