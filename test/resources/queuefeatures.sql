DROP TABLE IF EXISTS queuefeatures CASCADE;

CREATE TABLE queuefeatures (
    id SERIAL PRIMARY KEY,
    name VARCHAR(128)
);