package model

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.json.Json
import play.api.test.{FakeApplication, PlaySpecification, WithApplication}

class RightSpec extends PlaySpecification with DbTest {

  var userid1 = 0
  var userid2 = 0
  var userid3 = 0
  val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  def setup = {
    List("rights", "users").foreach(DBUtil.cleanTable(_))
    userid1 = Utils.insertUser("login1", "admin")
    userid2 = Utils.insertUser("login2", "supervisor")
    userid3 = Utils.insertUser("login3", "teacher", Some(formatter.parseDateTime("2014-06-02 00:00:00")), Some(formatter.parseDateTime("2014-09-12 23:59:59")))
  }

  "The Right singleton" should {
    "return an Admin right" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Right.forUser("login1") shouldEqual Some(AdminRight())
    }

    "return a Supervisor right" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertGroupRight(userid2, 2)
      Utils.insertGroupRight(userid2, 3)
      Utils.insertQueueRight(userid2, 4)
      Utils.insertQueueRight(userid2, 5)
      Utils.insertIncallRight(userid2, 6)
      Utils.insertIncallRight(userid2, 7)

      Right.forUser("login2") shouldEqual Some(SupervisorRight(List(4, 5), List(2, 3), List(6, 7)))
    }

    "return a Teacher right" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertGroupRight(userid3, 2)
      Utils.insertGroupRight(userid3, 3)
      Utils.insertQueueRight(userid3, 4)
      Utils.insertQueueRight(userid3, 5)
      Utils.insertIncallRight(userid3, 6)
      Utils.insertIncallRight(userid3, 7)

      Right.forUser("login3") shouldEqual Some(TeacherRight(List(4, 5), List(2, 3), List(6, 7), "2014-06-02", "2014-09-12"))
    }

    "return None for a non existing user" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      Right.forUser("loginXXX") shouldEqual None
    }

    "parse an AdminRight fro its JSON representation" in {
      Right.fromJson(Json.toJson(Json.obj("type" -> "admin", "data" -> Json.obj()))) shouldEqual AdminRight()
    }

    "parse a Supervisor Right from its JSON representation" in {
      Right.fromJson(Json.toJson(Json.obj(
        "type" -> "supervisor",
        "data" -> Json.obj("queueIds" -> List(1, 2), "groupIds" -> List(3, 4), "incallIds" -> List(5, 6))))) shouldEqual
          SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
    }

    "parse a TeacherRight from its JSON reporesentation" in {
      Right.fromJson(Json.toJson(Json.obj(
        "type" -> "teacher",
        "data" -> Json.obj("queueIds" -> List(1, 2), "groupIds" -> List(3, 4), "incallIds" -> List(5, 6), "start" -> "2014-01-08", "end" -> "2014-08-07")))) shouldEqual
        TeacherRight(List(1, 2), List(3, 4), List(5, 6), formatter.parseDateTime("2014-01-08 00:00:00"), formatter.parseDateTime("2014-08-07 23:59:59"))
    }

    "create a new user with admin rights" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login3", AdminRight())
      Right.forUser("login3") shouldEqual Some(AdminRight())
    }

    "create a new user with supervisor rights" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login3", SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))
      Right.forUser("login3") shouldEqual Some(SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))
    }

    "create a new user with teacher rights" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", TeacherRight(List(1, 2), List(3, 4), List(5, 6), "2014-02-01", "2014-06-12"))
      Right.forUser("login2") shouldEqual Some(TeacherRight(List(1, 2), List(3, 4), List(5, 6), "2014-02-01", "2014-06-12"))
    }

    "replace an existing supervisor with admin rights" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", AdminRight())
      Right.forUser("login2") shouldEqual Some(AdminRight())
    }

    "delete an admin right" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", AdminRight())

      Right.deleteByLogin("login2")

      Right.forUser("login2") shouldEqual None
    }

    "delete a supervisor right" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))

      Right.deleteByLogin("login2")

      Right.forUser("login2") shouldEqual None
    }

    "allow a Supervisor to create a Teacher with the same rights" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))

      Right.userCanCreateRight("login2", TeacherRight(List(1, 2), List(3, 4), List(5, 6), new DateTime(), new DateTime())) should beTrue
    }

    "allow a Supervisor to create a Teacher with more restrictive rights" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))

      Right.userCanCreateRight("login2", TeacherRight(List(1), List(3), List(5), new DateTime(), new DateTime())) should beTrue
    }

    "forbid a Supervisor to create a Supervisor or Admin right" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))

      Right.userCanCreateRight("login2", SupervisorRight(List(1, 2), List(3, 4), List(5, 6))) should beFalse
      Right.userCanCreateRight("login2", AdminRight()) should beFalse
    }

    "forbid a Supervisor to create a Teacher with rights on other objects" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))

      Right.userCanCreateRight("login2", TeacherRight(List(1, 3), List(3, 4), List(5, 6), new DateTime(), new DateTime())) should beFalse
    }

    "forbid an Admin or a Teacher to create a right" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))

      Right.userCanModifyUser("login1", "login4") should beFalse
      Right.userCanModifyUser("login3", "login4")should beFalse
    }

    "forbid a Supervisor to downgrade a Supervisor or an Admin to Teacher" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      setup
      Right.createOrReplace("login2", SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))

      Right.userCanModifyUser("login2", "login1") should beFalse
      Right.userCanModifyUser("login2", "login2") should beFalse
    }

  }

  "The AdminRight class" should {
    "jsonify itself" in {
      AdminRight().toJson shouldEqual Json.obj("type" -> "admin", "data" -> Json.obj())
    }
  }

  "The SupervisorRight class" should {
    "jsonify itself" in {
      SupervisorRight(List(1, 2), List(3, 4), List(5, 6)).toJson shouldEqual Json.obj("type" -> "supervisor",
        "data" -> Json.obj("queueIds" -> List(1, 2), "groupIds" -> List(3, 4), "incallIds" -> List(5, 6)))
    }
  }

  "The TeacherRight class" should {
    "jsonify itself" in {
      val start = formatter.parseDateTime("2014-01-01 00:00:00")
      val end = formatter.parseDateTime("2014-02-01 23:59:59")
      TeacherRight(List(1, 2), List(3, 4), List(5, 6), start, end).toJson shouldEqual Json.obj("type" -> "teacher",
        "data" -> Json.obj("queueIds" -> List(1, 2), "groupIds" -> List(3, 4), "incallIds" -> List(5, 6),
        "start" -> "2014-01-01", "end" -> "2014-02-01"))
    }
  }

}
