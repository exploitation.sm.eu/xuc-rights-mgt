package model
import anorm.SQL
import java.sql.Connection

import org.joda.time.DateTime

object Utils {
  def insertIncallRight(userId: Int, incallId: Int)(implicit c: Connection) = insertGenericRight(userId, "incall", incallId)

  def insertIncall(id: Int, number: String)(implicit c: Connection) =
    SQL("INSERT INTO incall(id, exten) VALUES ({id}, {number})").on('id -> id, 'number -> number).executeUpdate()

  def insertXivoUser(firstName: String, lastName: String, login: Option[String])(implicit c: Connection) =
    SQL("INSERT INTO userfeatures(loginclient, lastname, firstname) VALUES ({login}, {lastname}, {firstname})")
    .on('login -> login, 'lastname -> lastName, 'firstname -> firstName).executeUpdate()

  def insertQueue(id: Int, name: String)(implicit c: Connection) =
    SQL("INSERT INTO queuefeatures(id, name) VALUES ({id}, {name})").on('id -> id, 'name -> name).executeUpdate()

  def insertAgentGroup(id: Int, name: String)(implicit c: Connection) =
    SQL("INSERT INTO agentgroup(id, name) VALUES ({id}, {name})").on('id -> id, 'name -> name).executeUpdate()

  def insertGroupRight(userId: Int, groupId: Int)(implicit c: Connection) = insertGenericRight(userId, "agentgroup", groupId)

  def insertQueueRight(userId: Int, queueId: Int)(implicit c: Connection) = insertGenericRight(userId, "queue", queueId)

  def insertUser(login: String, userType: String, start: Option[DateTime]=None, end: Option[DateTime]=None)(implicit c: Connection): Int =
    SQL("INSERT INTO users(login, type, validity_start, validity_end) VALUES ({login}, {type}::user_type, {start}, {end})")
    .on('login -> login, 'type -> userType, 'start -> start.map(_.toDate), 'end -> end.map(_.toDate)).executeInsert[Option[Long]]().get.toInt

  def insertUser(user: User)(implicit c: Connection): Int = {
    SQL("INSERT INTO userfeatures(loginclient, lastname, firstname) VALUES ({login}, {name}, {firstname})")
      .on('login -> user.login, 'name -> user.name, 'firstname -> user.firstName).executeUpdate()
    SQL("INSERT INTO users(login, type) VALUES ({login}, {type}::user_type)")
      .on('login -> user.login, 'type -> user.profile).executeInsert[Option[Long]]().get.toInt
  }

  private def insertGenericRight(userId: Int, category: String, categoryId: Int)(implicit c: Connection) =
    SQL("INSERT INTO rights(user_id, category, category_id) VALUES ({userId}, {category}::right_type, {categoryId})")
    .on('userId -> userId, 'category -> category, 'categoryId -> categoryId).executeUpdate()
}
