package model

trait DbTest {

  implicit val connection = DBUtil.getConnection()
  val testConfig = Map(
    "db.default.url" -> "jdbc:postgresql://localhost/asterisktest",
    "db.default.user"-> "asterisk",
    "db.default.password" -> "asterisk",
    "db.xivo.url" -> "jdbc:postgresql://localhost/asterisktest",
    "db.xivo.user"-> "asterisk",
    "db.xivo.password" -> "asterisk",
    "evolutionplugin" -> "disabled")
  DBUtil.setupDB("rights.xml")

}

