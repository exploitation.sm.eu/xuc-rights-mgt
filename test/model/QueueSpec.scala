package model

import play.api.test.{FakeApplication, WithApplication, PlaySpecification}

class QueueSpec extends PlaySpecification with DbTest {

  def setup = DBUtil.cleanTable("queuefeatures")

   "The Queue singleton" should {
     "retrieve all queues" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
       setup
       Utils.insertQueue(1, "queue1")
       Utils.insertQueue(2, "queue2")

       Queue.all() shouldEqual List(Queue(Some(1), "queue1"), Queue(Some(2), "queue2"))
     }

     "retrieve only a few queues" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
       setup
       Utils.insertQueue(1, "queue1")
       Utils.insertQueue(2, "queue2")

       Queue.withIds(List(1)) shouldEqual List(Queue(Some(1), "queue1"))
     }

     "not fail with an empty list" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
       setup

       Queue.withIds(List()) shouldEqual List()
     }
   }
 }
