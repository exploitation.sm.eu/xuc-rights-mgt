package model

import play.api.test.{FakeApplication, WithApplication, PlaySpecification}

class IncallSpec extends PlaySpecification with DbTest{

  def setup = DBUtil.cleanTable("incall")

  "The Incall singleton" should {
    "retrieve all incalls" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertIncall(1, "5301")
      Utils.insertIncall(2, "5302")

      Incall.all() shouldEqual List(Incall(Some(1), "5301"), Incall(Some(2), "5302"))
    }

    "retrieve only some incalls" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertIncall(1, "5301")
      Utils.insertIncall(2, "5302")

      Incall.withIds(List(1)) shouldEqual List(Incall(Some(1), "5301"))
    }

    "not fail with an empty list" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Incall.withIds(List()) shouldEqual List()
    }
  }
}
