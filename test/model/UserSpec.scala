package model

import play.api.test.{FakeApplication, WithApplication, PlaySpecification}

class UserSpec extends PlaySpecification with DbTest {

  "The User singleton" should {
    "retrieve a full User" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      List("users", "userfeatures").foreach(DBUtil.cleanTable(_))
      val user1 = User("Dupond", "Pierre", "pdupond", "admin")
      val user2 = User("Dupont", "Jean", "jdupont", "supervisor")
      Utils.insertUser(user1)
      Utils.insertUser(user2)

      User.list shouldEqual List(user1, user2)
    }
  }
}
