package model

import play.api.test.{FakeApplication, WithApplication, PlaySpecification}

class XivoUserSpec extends PlaySpecification with DbTest {

  "The XivoUser singleton" should {
    "retrieve all xivo users with not null login" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      DBUtil.cleanTable("userfeatures")

      Utils.insertXivoUser("Pierre", "Dupond", Some("pdupond"))
      Utils.insertXivoUser("Jean", "Dupont", Some("jdupont"))
      Utils.insertXivoUser("Marc", "Dupondt", None)
      Utils.insertXivoUser("Martin", "Thomas", Some(""))

      XivoUser.all shouldEqual List(XivoUser("Pierre", Some("Dupond"), "pdupond"), XivoUser("Jean", Some("Dupont"), "jdupont"))
    }
  }
}
