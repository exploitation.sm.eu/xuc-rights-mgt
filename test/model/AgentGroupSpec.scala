package model

import play.api.test.{FakeApplication, WithApplication, PlaySpecification}

class AgentGroupSpec extends PlaySpecification with DbTest{

  "The AgentGroup singleton" should {

    def setup = DBUtil.cleanTable("agentgroup")

    "retrieve all agent groups" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertAgentGroup(1, "default")
      Utils.insertAgentGroup(2, "autre")

      AgentGroup.all() shouldEqual List(AgentGroup(Some(1), "default"), AgentGroup(Some(2), "autre"))
    }

    "retrieve only a few groups" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertAgentGroup(1, "default")
      Utils.insertAgentGroup(2, "autre")

      AgentGroup.withIds(List(1)) shouldEqual List(AgentGroup(Some(1), "default"))
    }

    "not fail with an empty list" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup

      AgentGroup.withIds(List()) shouldEqual List()
    }
  }
}
