package integration

import controllers.routes
import model.{AdminRight, Utils, DBUtil, DbTest}
import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.test.{FakeRequest, FakeApplication, WithApplication, PlaySpecification}

class RightsSpec extends PlaySpecification with DbTest {

  "The Rights Controller" should {
    "return the rights for a user" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      DBUtil.cleanTable("users")
      Utils.insertUser("login1", "admin")
      val res = route(FakeRequest(routes.Rights.getRights("login1"))).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj("type" -> "admin", "data" -> Json.obj())
    }

    "return 404 if there is no right for this user" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      DBUtil.cleanTable("users")

      val res = route(FakeRequest(routes.Rights.getRights("login1"))).get

      status(res) shouldEqual NOT_FOUND
    }

    "create a new right" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      DBUtil.cleanTable("users")

      val res = route(FakeRequest(POST, "/rights/user/login1")
        .withJsonBody(Json.obj("type" -> "admin", "data" -> Json.obj()))
        .withSession("username" -> "test", "isSuperAdmin" -> "true")).get

      status(res) shouldEqual OK
      model.Right.forUser("login1") shouldEqual Some(AdminRight())
    }

    "delete a right" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      DBUtil.cleanTable("users")
      Utils.insertUser("login1", "admin")

      val res = route(FakeRequest(DELETE, "/rights/user/login1").withSession("username" -> "test", "isSuperAdmin" -> "true")).get

      status(res) shouldEqual OK
    }

    "check the user's right before doing modifications" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      DBUtil.cleanTable("users")
      Utils.insertUser("login1", "teacher", Some(new DateTime()), Some(new DateTime()))
      Utils.insertUser("login2", "supervisor")

      status(route(FakeRequest(POST, "/rights/user/login1")
        .withJsonBody(Json.obj("type" -> "admin"))
        .withSession("username" -> "login2")).get) shouldEqual BAD_REQUEST

      status(route(FakeRequest(POST, "/rights/user/login1")
        .withJsonBody(Json.obj("type" -> "admin"))
        .withSession("username" -> "test", "isSuperAdmin" -> "true")).get) shouldEqual OK
    }

    "check the user's right before deleting" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      DBUtil.cleanTable("users")
      Utils.insertUser("login1", "teacher", Some(new DateTime()), Some(new DateTime()))
      Utils.insertUser("login2", "supervisor")

      status(route(FakeRequest(DELETE, "/rights/user/login1").withSession("username" -> "login2")).get) shouldEqual OK
      status(route(FakeRequest(DELETE, "/rights/user/login2").withSession("username" -> "login2")).get) shouldEqual BAD_REQUEST
      status(route(FakeRequest(DELETE, "/rights/user/login2").withSession("username" -> "test", "isSuperAdmin" -> "true")).get) shouldEqual OK
    }
  }
}
