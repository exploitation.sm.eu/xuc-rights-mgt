package integration

import controllers.routes
import model.DbTest
import play.api.libs.json.Json
import play.api.test.{FakeRequest, FakeApplication, WithApplication, PlaySpecification}
import model.{DBUtil, Utils}

class XivoObjectsSpec extends PlaySpecification with DbTest {

  def setup = {
    List("queuefeatures", "users", "rights", "incall", "agentgroup").foreach(DBUtil.cleanTable(_))
  }

  "The XivoObjects Controller" should {
    "return all queues for a superadmin" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertQueue(1, "queue1")
      Utils.insertQueue(2, "queue2")
      val res = route(FakeRequest(routes.XivoObjects.getQueues).withSession("username" -> "test", "isSuperAdmin" -> "true")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List(Json.obj("id" -> 1, "name" -> "queue1"), Json.obj("id" -> 2, "name" -> "queue2")))
    }

    "return only the user's queues for a supervisor" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      val userid = Utils.insertUser("login1", "supervisor")
      Utils.insertQueueRight(userid, 2)
      Utils.insertQueue(2, "thequeue")
      Utils.insertQueue(3, "anotherqueue")

      val res = route(FakeRequest(routes.XivoObjects.getQueues).withSession("username" -> "login1")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List(Json.obj("id" -> 2, "name" -> "thequeue")))
    }

    "return all agent groups for a superadmin" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertAgentGroup(1, "group1")
      Utils.insertAgentGroup(2, "group2")
      val res = route(FakeRequest(routes.XivoObjects.getGroups).withSession("username" -> "test", "isSuperAdmin" -> "true")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List(Json.obj("id" -> 1, "name" -> "group1"), Json.obj("id" -> 2, "name" -> "group2")))
    }

    "return only the user's groups for a supervisor" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      val userid = Utils.insertUser("login1", "supervisor")
      Utils.insertGroupRight(userid, 2)
      Utils.insertAgentGroup(2, "thegroup")
      Utils.insertAgentGroup(3, "anothergroup")

      val res = route(FakeRequest(routes.XivoObjects.getGroups).withSession("username" -> "login1")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List(Json.obj("id" -> 2, "name" -> "thegroup")))
    }

    "return all xivo users" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertXivoUser("Pierre", "Martin", Some("pmartin"))

      val res = route(FakeRequest(routes.XivoObjects.getXivoUsers).withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List(Json.obj(
      "firstname" -> "Pierre", "lastname" -> "Martin", "login" -> "pmartin"
      )))
    }

    "return all incalls for a superadmin" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      Utils.insertIncall(1, "5301")
      Utils.insertIncall(2, "5302")
      val res = route(FakeRequest(routes.XivoObjects.getIncalls).withSession("username" -> "test", "isSuperAdmin" -> "true")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List(Json.obj("id" -> 1, "number" -> "5301"), Json.obj("id" -> 2, "number" -> "5302")))
    }

    "return only the user's incalls for a supervisor" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      setup
      val userid = Utils.insertUser("login1", "supervisor")
      Utils.insertIncallRight(userid, 2)
      Utils.insertIncall(2, "5003")
      Utils.insertIncall(3, "5004")

      val res = route(FakeRequest(routes.XivoObjects.getIncalls).withSession("username" -> "login1")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List(Json.obj("id" -> 2, "number" -> "5003")))
    }
  }
}
