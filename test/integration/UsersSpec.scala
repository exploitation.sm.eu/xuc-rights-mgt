package integration

import controllers.routes
import model.{Utils, User, DbTest}
import play.api.libs.json.Json
import play.api.test.{FakeRequest, FakeApplication, WithApplication, PlaySpecification}

class UsersSpec extends PlaySpecification with DbTest {

  "The Users Controller" should {
    "list users" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)){
      Utils.insertUser(User("Dupond", "Pierre", "pdupond", "admin"))
      Utils.insertUser(User("Dupont", "Jean", "jdupont", "supervisor"))

      val res = route(FakeRequest(routes.Users.all).withSession("username" -> "test")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(Json.obj(
          "name" -> "Dupond",
          "firstname" -> "Pierre",
          "login" -> "pdupond",
          "profile" -> "admin"),
            Json.obj(
            "name" -> "Dupont",
            "firstname" -> "Jean",
            "login" -> "jdupont",
            "profile" -> "supervisor"
            )
        ))
    }
  }
}
