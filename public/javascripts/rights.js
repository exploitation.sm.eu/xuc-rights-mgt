function profileLabel(profile) {
    if(profile == 'admin')
        return 'Administrateur';
    if(profile == 'supervisor')
        return 'Superviseur';
    if(profile == 'teacher')
        return 'Formateur';
    return '';
}

angular.module('users', ['ui.bootstrap', 'checklist-model'])
.controller('UsersController', function($scope, $modal) {

    $scope.displayError = function(response) {
        $scope.error = response.responseJSON;
        $scope.$apply();
    }

    $scope.processUsers = function(json) {
        $scope.users = json;
        $scope.$apply();
    }

    $scope.processXivoUsers = function(json) {
        $scope.xivoUsers = json;
        $scope.$apply();
    }

    $scope.getUsers = function() {
    $scope.error = undefined;
        $.ajax({
            type : "GET",
            url : '/users',
            success: $scope.processUsers
        });
    }

    $scope.getXivoUsers = function() {
        $.ajax({
            type : "GET",
            url : '/xivo_users',
            success: $scope.processXivoUsers
        });
    }

    $scope.editUser = function(user) {
        $modal.open({
          templateUrl: 'modal.html',
          controller: 'ModalController',
          resolve: {
            user: function() {
                return user;
            }
          }
        }).result.then($scope.getUsers);
    }

    $scope.deleteUser = function(user) {
        $.ajax({
            type : "DELETE",
            url : '/rights/user/' + user.login,
            success: $scope.getUsers,
            error: $scope.displayError
        });
    }

    $scope.fullName = function(user) {
        if(user)
            return user.firstname + ' ' + user.lastname + ' (' + user.login + ')';
    }

    $scope.createUser = function(xivoUser) {
        var user = {
            name: xivoUser.lastname,
            firstname: xivoUser.firstname,
            login: xivoUser.login,
            profile: 'admin'
        }
        $scope.editUser(user);
    }

    $scope.profileLabel = profileLabel;
    $scope.getUsers();
    $scope.getXivoUsers();
})
.controller('ModalController', function($scope, $modalInstance, user) {

    $scope.fermer = $modalInstance.dismiss;

    $scope.displayError = function(response) {
        $scope.error = response.responseJSON;
        $scope.$apply();
    }

    $scope.enregistrer = function() {
        $.ajax({
            type : "POST",
            contentType: 'application/json',
            data: JSON.stringify($scope.right),
            url : '/rights/user/' + $scope.user.login,
            success: $modalInstance.close,
            error: $scope.displayError
        });
    }

    $scope.newRight = function(response) {
        $scope.right = {
            type: 'admin'
        }
        $scope.$apply();
    }

    $scope.getRight = function(login) {
        $.ajax({
            type : "GET",
            url : '/rights/user/' + login,
            success: function(json) {
                $scope.right = json;
                $scope.$apply();
            },
            error: $scope.newRight
        });
    }

    $scope.profileLabel = profileLabel;

    $scope.getQueues = function () {
        $.ajax({
            type : "GET",
            url : '/queues',
            success: function(json) {
                $scope.queues = json;
                $scope.getGroups();
            }
        });
    }

    $scope.getGroups = function () {
        $.ajax({
            type : "GET",
            url : '/groups',
            success: function(json) {
                $scope.groups = json;
                $scope.getIncalls();
            }
        });
    }

    $scope.getIncalls = function() {
        $.ajax({
            type : "GET",
            url : '/incalls',
            success: function(json) {
                $scope.incalls = json;
                $scope.getRight(user.login);
            }
        });
    }

    $scope.$$postDigest(function() {
        $('#start').datetimepicker({
            language: 'fr',
            pickTime: false
        });

        $('#end').datetimepicker({
            language: 'fr',
            pickTime: false
        });
    });

    $scope.profiles = ['admin', 'supervisor', 'teacher'];
    $scope.user = user;
    $scope.getQueues();
});